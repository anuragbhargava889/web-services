<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderNew;
use App\Http\Requests\OrderRead;
use App\Http\Requests\OrderUpdate;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Order;
use GoogleMaps\Facade\GoogleMapsFacade as GoogleMaps;
use PHPUnit\Runner\Exception;

class OrderController extends Controller
{
    /**
     * @param OrderRead $request
     *
     * @return OrderCollection
     */
    public function index(OrderRead $request)
    {
        try {
            $limit = $request->query('limit', 20);

            return new OrderCollection(OrderResource::collection(Order::Paginate($limit)));
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param OrderNew $request
     *
     * @return OrderResource|\Illuminate\Http\JsonResponse
     */
    public function store(OrderNew $request)
    {
        try {
            $distance = $this->getDistanceFromGoogle($request->origin, $request->destination);
            if ($distance[0]['status'] === 'OK') {
                $order = Order::create(
                    [
                        'origin'      => $request->origin,
                        'destination' => $request->destination,
                        'distance'    => $distance[0]['distance']['value'],
                    ]
                );
                $order = $order->refresh();

                return new OrderResource($order);
            } else {
                return response()->json(['error' => 'Google_api_issue'], 409);
            }
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param OrderUpdate $request
     * @param Order       $order
     *
     * @return OrderResource|\Illuminate\Http\JsonResponse
     */
    public function update(OrderUpdate $request, Order $order)
    {
        try {
            if (strtoupper($request->status) === strtoupper($order->status)) {
                return response()->json(['error' => 'ORDER_ALREADY_BEEN_TAKEN'], 409);
            }

            $order->update($request->only(['status']));

            return response()->json(['status' => 'SUCCESS'], 200);
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * @param $origin
     * @param $destination
     *
     * @return string
     */
    private function getDistanceFromGoogle($origin, $destination)
    {
        $distance = GoogleMaps::load('distancematrix')
                              ->setParam(
                                  [
                                      'origins'      => implode(',', $origin),
                                      'destinations' => implode(',', $destination),
                                  ]
                              )->get('rows');

        return $distance['rows'][0]['elements'] ?? null;
    }
}
