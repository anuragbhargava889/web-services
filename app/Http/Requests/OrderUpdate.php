<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\OrderSchema;

class OrderUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*' => new OrderSchema,
            'status' => 'required|string|in:TAKEN,UNASSIGN'
        ];
    }

    public function messages()
    {
        return [
            'status.in' => 'Status Should be TAKEN and UNASSIGN and in uppercase'
        ];
    }
}
