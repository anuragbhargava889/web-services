<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\OrderSchema;

class OrderNew extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*'             => new OrderSchema,
            'origin'        => 'required|array|min:2|max:2',
            'origin.0'      => 'required|string',
            'origin.1'      => 'required|string',
            'destination'   => 'required|array|min:2|max:2',
            'destination.0' => 'required|string',
            'destination.1' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'origin.0.required'      => 'Origin START_LATITUDE should not be empty.',
            'origin.0.string'        => 'Origin START_LATITUDE should be string.',
            'origin.1.required'      => 'Origin START_LONGTITUDE should not be empty.',
            'origin.1.string'        => 'Origin START_LONGTITUDE should be string.',
            'destination.0.required' => 'Destination END_LATITUDE should not be empty.',
            'destination.0.string'   => 'Destination END_LATITUDE should be string.',
            'destination.1.required' => 'Destination END_LONGTITUDE should not be empty.',
            'destination.1.string'   => 'Destination END_LONGTITUDE should be string.',
        ];
    }
}
