<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * @var string
     */
    protected $table = 'orders';

    /**
     * @var array
     */
    protected $fillable = [
        'origin',
        'destination',
        'status',
        'distance',
    ];

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getOriginAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     */
    public function setOriginAttribute($value)
    {
        $this->attributes['origin'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getDestinationAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     */
    public function setDestinationAttribute($value)
    {
        $this->attributes['destination'] = serialize($value);
    }

    /**
     * @param $value
     */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = strtoupper($value);
    }

}
