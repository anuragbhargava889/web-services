<?php

use Illuminate\Database\Seeder;
use App\Order as Order;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order              = new Order();
        $order->origin      = [28.4595, 77.0266];
        $order->destination = [28.5355, 77.3910];
        $order->save();
    }
}
