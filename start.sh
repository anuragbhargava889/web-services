#!/usr/bin/env bash

set -e

echo Give permission to mysql mount directory
sudo chmod 777 -R MYSQL-V/

 [ -f ".env.testing" ] || $(echo Please make an .env.testing file and run: php artisan key:generate --env=testing; exit 1)

export $(cat .env.testing | grep -v ^# | xargs);
echo Starting services
sudo docker-compose up -d
echo Host: 127.0.0.1
until sudo docker-compose exec mysql mysql -h 127.0.0.1 -u $DB_USERNAME -p$DB_PASSWORD -D $DB_DATABASE --silent -e "show databases;"
do
  echo "Waiting for database connection..."
  sleep 5
done

echo Give permission to storage
sudo docker-compose exec app chmod 777 -R storage/

echo Give permission to mysql mount directory
sudo chmod 777 -R MYSQL-V/

echo Installing dependencies
./scripts/composer.sh install

echo removing cache files
sudo rm -f bootstrap/cache/*.php

sudo docker-compose exec app php artisan migrate --env=testing && echo Database migrated
