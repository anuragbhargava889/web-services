<?php
namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use PHPUnit\Framework\TestResult;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use WithoutMiddleware;

    public function testOrders()
    {
        echo "\n \n Starts Executing Unit Test Cases \n \n";
        $response = $this->json('GET', '/api/orders?page=1&limit=10');
        $response->assertStatus(200);
        echo "\n \n GET Orders test case passed \n \n";
    }
}
